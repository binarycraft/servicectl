const std = @import("std");
const os = std.os;
const fmt = std.fmt;
const mem = std.mem;
const log = std.log;
const time = std.time;
const print = std.debug.print;
const unicode = std.unicode;
const winapi = @import("winapi.zig");
const windows = os.windows;

const g = struct {
    var SvcName: [:0]u16 = undefined;
    var SvcStatusHandle: winapi.SERVICE_STATUS_HANDLE = undefined;
    var hSvcStopEvent: windows.HANDLE = undefined;
    var SvcStatus: winapi.SERVICE_STATUS = undefined;
    var SzPath: [:0]u16 = undefined;
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const argv = try std.process.argsAlloc(gpa.allocator());
    defer std.process.argsFree(gpa.allocator(), argv);

    if (argv.len < 3) {
        SvcCtrlUsage(argv);
        return;
    } else if (mem.eql(u8, mem.span(argv[1]), "add") and
        argv.len <= 3)
    {
        SvcCtrlUsage(argv);
        return;
    }

    g.SvcName = try unicode.utf8ToUtf16LeWithNull(
        gpa.allocator(),
        argv[2],
    );
    defer gpa.allocator().free(g.SvcName);

    if (mem.eql(u8, mem.span(argv[1]), "run")) {
        try SvsRun(gpa.allocator());
    } else if (mem.eql(u8, mem.span(argv[1]), "add")) {
        var allocator = gpa.allocator();
        const SvsArg = try mem.join(allocator, " ", argv[3..]);
        defer allocator.free(SvsArg);

        const SzPath = try fmt.allocPrint(
            allocator,
            "{s} run {s} {s}",
            .{
                argv[0],
                argv[2],
                SvsArg,
            },
        );
        defer allocator.free(SzPath);

        g.SzPath = try unicode.utf8ToUtf16LeWithNull(
            allocator,
            SzPath,
        );
        defer allocator.free(g.SzPath);
        try SvcAdd();
    } else if (mem.eql(u8, mem.span(argv[1]), "del")) {
        try SvcDel();
    } else if (mem.eql(u8, mem.span(argv[1]), "start")) {
        try DoStartSvc();
    } else if (mem.eql(u8, mem.span(argv[1]), "stop")) {
        try DoStopSvc();
    } else if (mem.eql(u8, mem.span(argv[1]), "restart")) {
        try DoStopSvc();
        try DoStartSvc();
    } else if (mem.eql(u8, mem.span(argv[1]), "status")) {
        try SvcStatus(gpa.allocator());
    } else if (mem.eql(u8, mem.span(argv[1]), "disable")) {
        try SvcDisable(gpa.allocator());
    } else if (mem.eql(u8, mem.span(argv[1]), "enable")) {
        try SvcEnable(gpa.allocator());
    } else {
        SvcCtrlUsage(argv);
    }
}

pub fn SvcCtrlUsage(argv: []const []const u8) void {
    print("{s} COMMAND ...\n\n", .{argv[0]});
    print("Query or send control commands to the service manager.\n\n", .{});
    print("Service Commands:\n", .{});
    print(" start [SERVICE]             Start (activate) the service\n", .{});
    print(" stop [SERVICE]              Stop (deactivate) the service\n", .{});
    print(" restart [SERVICE]           Restart the service\n", .{});
    print(" status [SERVICE]            Show runtime status of the service\n", .{});
    print(" enable [SERVICE]            Enable the service\n", .{});
    print(" disable [SERVICE]           Disable the service\n", .{});
    print(" add [SERVICE] [CWD] [CMD]   add [CMD] as a service\n", .{});
    print(" del [SERVICE]               Delete the service\n", .{});
}

pub fn SvcEnable(allocator: mem.Allocator) !void {
    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    var SvcName = try unicode.utf16leToUtf8Alloc(
        allocator,
        g.SvcName,
    );
    defer allocator.free(SvcName);

    if (winapi.ChangeServiceConfigW(
        schService,
        winapi.SERVICE_NO_CHANGE,
        winapi.SERVICE_AUTO_START,
        winapi.SERVICE_NO_CHANGE,
        null, // binary path: no change
        null, // load order group: no change
        null, // tag ID: no change
        null, // dependencies: no change
        null, // account name: no change
        null, // password: no change
        null, // display name: no change
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("ChangeServiceConfig failed {d}\n", .{@enumToInt(err)});
    } else {
        print("Service: {s} enabled successfully.\n", .{SvcName});
    }
}

pub fn SvcDisable(allocator: mem.Allocator) !void {
    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    var SvcName = try unicode.utf16leToUtf8Alloc(
        allocator,
        g.SvcName,
    );
    defer allocator.free(SvcName);

    if (winapi.ChangeServiceConfigW(
        schService,
        winapi.SERVICE_NO_CHANGE,
        winapi.SERVICE_DEMAND_START,
        winapi.SERVICE_NO_CHANGE,
        null, // binary path: no change
        null, // load order group: no change
        null, // tag ID: no change
        null, // dependencies: no change
        null, // account name: no change
        null, // password: no change
        null, // display name: no change
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("ChangeServiceConfig failed {d}\n", .{@enumToInt(err)});
    } else {
        print("Service: {s} disabled successfully.\n", .{SvcName});
    }
}

pub fn SvcStatus(allocator: mem.Allocator) !void {
    var ssStatus: winapi.SERVICE_STATUS_PROCESS = undefined;
    var cbBufSize: windows.DWORD = undefined;
    var dwBytesNeeded: windows.DWORD = undefined;
    var lpsc: []winapi.QUERY_SERVICE_CONFIGW = undefined;
    var lpsd: []winapi.SERVICE_DESCRIPTIONW = undefined;

    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    if (winapi.QueryServiceStatusEx(
        schService,
        winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
        @ptrCast(*u8, &ssStatus),
        @sizeOf(winapi.SERVICE_STATUS_PROCESS),
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
        return;
    }

    if (winapi.QueryServiceConfigW(
        schService,
        null,
        0,
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const dwError = windows.kernel32.GetLastError();
        if (windows.Win32Error.INSUFFICIENT_BUFFER == dwError) {
            cbBufSize = dwBytesNeeded;
            const lpscLocal = try allocator.alloc(
                winapi.QUERY_SERVICE_CONFIGW,
                cbBufSize,
            );
            defer allocator.free(lpscLocal);

            lpsc = try allocator.dupe(
                winapi.QUERY_SERVICE_CONFIGW,
                lpscLocal,
            );
        } else {
            print("QueryServiceConfig failed {d}", .{dwError});
            return;
        }
    }

    if (winapi.QueryServiceConfigW(
        schService,
        @ptrCast(*winapi.QUERY_SERVICE_CONFIGW, lpsc.ptr),
        cbBufSize,
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const dwError = windows.kernel32.GetLastError();
        print("QueryServiceConfig failed {d}", .{dwError});
        return;
    }

    if (winapi.QueryServiceConfig2W(
        schService,
        winapi.SERVICE_CONFIG_DESCRIPTION,
        null,
        0,
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const dwError = windows.kernel32.GetLastError();
        if (windows.Win32Error.INSUFFICIENT_BUFFER == dwError) {
            cbBufSize = dwBytesNeeded;
            const lpsdLocal = try allocator.alloc(
                winapi.SERVICE_DESCRIPTIONW,
                cbBufSize,
            );
            defer allocator.free(lpsdLocal);

            lpsd = try allocator.dupe(
                winapi.SERVICE_DESCRIPTIONW,
                lpsdLocal,
            );
        } else {
            print("QueryServiceConfig2 failed {d}", .{dwError});
            return;
        }
    }

    if (winapi.QueryServiceConfig2W(
        schService,
        winapi.SERVICE_CONFIG_DESCRIPTION,
        @ptrCast(*u8, lpsd.ptr),
        cbBufSize,
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const dwError = windows.kernel32.GetLastError();
        print("QueryServiceConfig failed {d}", .{dwError});
        return;
    }

    var SvcName = try unicode.utf16leToUtf8Alloc(
        allocator,
        g.SvcName,
    );
    defer allocator.free(SvcName);

    const StartType = switch (lpsc[0].dwStartType) {
        winapi.SERVICE_AUTO_START => "Enabled",
        else => "Disabled",
    };

    switch (ssStatus.dwCurrentState) {
        winapi.SERVICE_STOPPED => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Stopped\n", .{});
        },
        winapi.SERVICE_START_PENDING => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Start pending\n", .{});
        },
        winapi.SERVICE_STOP_PENDING => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Stop pending\n", .{});
        },
        winapi.SERVICE_RUNNING => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Running\n", .{});
        },
        winapi.SERVICE_CONTINUE_PENDING => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Continue pending\n", .{});
        },
        winapi.SERVICE_PAUSE_PENDING => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Pause pending\n", .{});
        },
        winapi.SERVICE_PAUSED => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Paused\n", .{});
        },
        else => {
            print("   Service: {s}\n", .{SvcName});
            print("Start Type: {s}\n", .{StartType});
            print("    Status: Unknown\n", .{});
        },
    }
}

pub fn DoStartSvc() !void {
    var ssStatus: winapi.SERVICE_STATUS_PROCESS = undefined;
    var dwBytesNeeded: windows.DWORD = undefined;
    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    if (winapi.QueryServiceStatusEx(
        schService,
        winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
        @ptrCast(*u8, &ssStatus),
        @sizeOf(winapi.SERVICE_STATUS_PROCESS),
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
        return;
    }

    if (ssStatus.dwCurrentState != winapi.SERVICE_STOPPED and
        ssStatus.dwCurrentState != winapi.SERVICE_STOP_PENDING)
    {
        print("Cannot start the service because it is already running\n", .{});
        return;
    }
    var dwStartTickCount = winapi.GetTickCount();
    var dwOldCheckPoint = ssStatus.dwCheckPoint;

    while (ssStatus.dwCurrentState == winapi.SERVICE_STOP_PENDING) {
        var dwWaitTime = ssStatus.dwWaitHint / 10;
        if (dwWaitTime < 1000) {
            dwWaitTime = 1000;
        } else if (dwWaitTime > 1000) {
            dwWaitTime = 1000;
        }
        std.time.sleep(dwWaitTime * 1000000);

        if (winapi.QueryServiceStatusEx(
            schService,
            winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
            @ptrCast(*u8, &ssStatus),
            @sizeOf(winapi.SERVICE_STATUS_PROCESS),
            &dwBytesNeeded,
        ) == windows.FALSE) {
            const err = windows.kernel32.GetLastError();
            print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
            return;
        }

        if (ssStatus.dwCheckPoint > dwOldCheckPoint) {
            dwStartTickCount = winapi.GetTickCount();
            dwOldCheckPoint = ssStatus.dwCheckPoint;
        } else {
            if (winapi.GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint) {
                print("Timeout waiting for service to stop\n", .{});
                return;
            }
        }
    }

    if (winapi.StartServiceW(schService, 0, null) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("StartService failed {d}\n", .{@enumToInt(err)});
    } else {
        print("Service start pending...\n", .{});
    }

    if (winapi.QueryServiceStatusEx(
        schService,
        winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
        @ptrCast(*u8, &ssStatus),
        @sizeOf(winapi.SERVICE_STATUS_PROCESS),
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
        return;
    }

    dwStartTickCount = winapi.GetTickCount();
    dwOldCheckPoint = ssStatus.dwCheckPoint;

    while (ssStatus.dwCurrentState == winapi.SERVICE_START_PENDING) {
        var dwWaitTime = ssStatus.dwWaitHint / 10;
        if (dwWaitTime < 1000) {
            dwWaitTime = 1000;
        } else if (dwWaitTime > 1000) {
            dwWaitTime = 1000;
        }
        std.time.sleep(dwWaitTime * 1000000);

        if (winapi.QueryServiceStatusEx(
            schService,
            winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
            @ptrCast(*u8, &ssStatus),
            @sizeOf(winapi.SERVICE_STATUS_PROCESS),
            &dwBytesNeeded,
        ) == windows.FALSE) {
            const err = windows.kernel32.GetLastError();
            print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
            return;
        }

        if (ssStatus.dwCheckPoint > dwOldCheckPoint) {
            dwStartTickCount = winapi.GetTickCount();
            dwOldCheckPoint = ssStatus.dwCheckPoint;
        } else {
            if (winapi.GetTickCount() - dwStartTickCount > ssStatus.dwWaitHint) {
                print("Timeout waiting for service to stop\n", .{});
                break;
            }
        }
    }

    if (ssStatus.dwCurrentState == winapi.SERVICE_RUNNING) {
        print("Service started successfully.\n", .{});
    } else {
        print("Service not started. \n", .{});
        print("  Current State: {d}\n", .{ssStatus.dwCurrentState});
        print("  Exit Code: {d}\n", .{ssStatus.dwWin32ExitCode});
        print("  Check Point: {d}\n", .{ssStatus.dwCheckPoint});
        print("  Wait Hint: {d}\n", .{ssStatus.dwWaitHint});
    }
}

pub fn DoStopSvc() !void {
    var ssStatus: winapi.SERVICE_STATUS_PROCESS = undefined;
    var dwBytesNeeded: windows.DWORD = undefined;
    var dwTimeout: windows.DWORD = 30000;
    var dwStartTime = winapi.GetTickCount();

    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    if (winapi.QueryServiceStatusEx(
        schService,
        winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
        @ptrCast(*u8, &ssStatus),
        @sizeOf(winapi.SERVICE_STATUS_PROCESS),
        &dwBytesNeeded,
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
        return;
    }

    if (ssStatus.dwCurrentState == winapi.SERVICE_STOPPED) {
        print("Service is already stopped.\n", .{});
        return;
    }

    while (ssStatus.dwCurrentState == winapi.SERVICE_STOP_PENDING) {
        var dwWaitTime = ssStatus.dwWaitHint / 10;
        if (dwWaitTime < 1000) {
            dwWaitTime = 1000;
        } else if (dwWaitTime > 1000) {
            dwWaitTime = 1000;
        }
        std.time.sleep(dwWaitTime * 1000000);

        if (winapi.QueryServiceStatusEx(
            schService,
            winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
            @ptrCast(*u8, &ssStatus),
            @sizeOf(winapi.SERVICE_STATUS_PROCESS),
            &dwBytesNeeded,
        ) == windows.FALSE) {
            const err = windows.kernel32.GetLastError();
            print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
            return;
        }
        if (ssStatus.dwCurrentState == winapi.SERVICE_STOPPED) {
            print("Service stopped successfully.\n", .{});
            return;
        }

        if (winapi.GetTickCount() - dwStartTime > dwTimeout) {
            print("Service stop timed out.\n", .{});
            return;
        }
    }
    // ToDO: Implement StopDependentServices
    // StopDependentServices();

    if (winapi.ControlService(
        schService,
        winapi.SERVICE_CONTROL_STOP,
        @ptrCast(*winapi.SERVICE_STATUS, &ssStatus),
    ) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        print("ControlService failed {d}\n", .{@enumToInt(err)});
        return;
    }

    while (ssStatus.dwCurrentState != winapi.SERVICE_STOPPED) {
        std.time.sleep(ssStatus.dwWaitHint * 1000000);

        if (winapi.QueryServiceStatusEx(
            schService,
            winapi.SC_STATUS_TYPE.SC_STATUS_PROCESS_INFO,
            @ptrCast(*u8, &ssStatus),
            @sizeOf(winapi.SERVICE_STATUS_PROCESS),
            &dwBytesNeeded,
        ) == windows.FALSE) {
            const err = windows.kernel32.GetLastError();
            print("QueryServiceStatusEx failed {d}\n", .{@enumToInt(err)});
            return;
        }
        if (ssStatus.dwCurrentState == winapi.SERVICE_STOPPED) {
            print("Service stopped successfully.\n", .{});
            return;
        }

        if (winapi.GetTickCount() - dwStartTime > dwTimeout) {
            print("Wait timed out.\n", .{});
            return;
        }
    }
    print("Service stopped successfully\n", .{});
}

pub fn SvcDel() !void {
    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.OpenServiceW(
        schSCManager,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);

    if (winapi.DeleteService(schService) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    }
}

pub fn SvcAdd() !void {
    var schSCManager = winapi.OpenSCManagerW(
        null,
        null,
        winapi.SC_MANAGER_ALL_ACCESS,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schSCManager);

    var schService = winapi.CreateServiceW(
        schSCManager,
        g.SvcName,
        g.SvcName,
        winapi.SERVICE_ALL_ACCESS,
        winapi.SERVICE_WIN32_OWN_PROCESS,
        winapi.SERVICE_DEMAND_START,
        winapi.SERVICE_ERROR_NORMAL,
        g.SzPath,
        null,
        null,
        null,
        null,
        null,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    };
    defer _ = winapi.CloseServiceHandle(schService);
}

pub fn SvsRun(allocator: mem.Allocator) !void {
    defer allocator.free(g.SvcName);

    var ServiceTable = [_]winapi.SERVICE_TABLE_ENTRYW{
        .{
            .lpServiceName = g.SvcName.ptr,
            .lpServiceProc = &SvcMain,
        },
        .{
            .lpServiceName = null,
            .lpServiceProc = null,
        },
    };
    if (winapi.StartServiceCtrlDispatcherW(&ServiceTable[0]) == windows.FALSE) {
        const err = windows.kernel32.GetLastError();
        return windows.unexpectedError(err);
    }
}

pub fn SvcMain(
    dwNumServicesArgs: windows.DWORD,
    lpServiceArgVectors: ?*windows.LPWSTR,
) callconv(windows.WINAPI) void {
    g.SvcStatusHandle = winapi.RegisterServiceCtrlHandlerW(
        g.SvcName.ptr,
        &SvcCtrlHandler,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        const errCode = @enumToInt(err);
        ReportSvcStatus(winapi.SERVICE_STOPPED, errCode, 0);
        return;
    };

    g.SvcStatus = .{
        .dwServiceType = 0,
        .dwCurrentState = 0,
        .dwControlsAccepted = 0,
        .dwWin32ExitCode = 0,
        .dwServiceSpecificExitCode = 0,
        .dwCheckPoint = 0,
        .dwWaitHint = 0,
    };

    g.SvcStatus.dwServiceType = winapi.SERVICE_WIN32_OWN_PROCESS;

    ReportSvcStatus(winapi.SERVICE_START_PENDING, 0, 3000);

    SvcInit(dwNumServicesArgs, lpServiceArgVectors);
}

pub fn SvcInit(
    dwNumServicesArgs: windows.DWORD,
    lpServiceArgVectors: ?*windows.LPWSTR,
) void {
    _ = dwNumServicesArgs;
    _ = lpServiceArgVectors;

    g.hSvcStopEvent = winapi.CreateEventW(
        null,
        windows.TRUE,
        windows.FALSE,
        null,
    ) orelse {
        const err = windows.kernel32.GetLastError();
        const errCode = @enumToInt(err);
        ReportSvcStatus(winapi.SERVICE_STOPPED, errCode, 0);
        return;
    };
    defer windows.CloseHandle(g.hSvcStopEvent);

    ReportSvcStatus(winapi.SERVICE_RUNNING, 0, 0);
    SvcWorker();
    ReportSvcStatus(winapi.SERVICE_STOPPED, 0, 0);
}

pub fn SvcCtrlHandler(
    dwCtrlCode: windows.DWORD,
) callconv(windows.WINAPI) void {
    switch (dwCtrlCode) {
        winapi.SERVICE_CONTROL_STOP => {
            ReportSvcStatus(winapi.SERVICE_STOP_PENDING, 0, 0);

            _ = winapi.SetEvent(g.hSvcStopEvent);
            ReportSvcStatus(g.SvcStatus.dwCurrentState, 0, 0);
            return;
        },
        else => {},
    }
}

pub fn SvcWorker() void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const argv = std.process.argsAlloc(gpa.allocator()) catch |err| {
        ReportSvcStatus(winapi.SERVICE_STOPPED, @errorToInt(err), 0);
        return;
    };
    defer std.process.argsFree(gpa.allocator(), argv);

    var cwd = argv[3];
    var child = std.ChildProcess.init(argv[4..], gpa.allocator());

    child.stdin_behavior = .Close;
    child.stdout_behavior = .Close;
    child.stderr_behavior = .Close;
    child.cwd = cwd;
    child.cwd_dir = null;
    child.env_map = null;
    child.expand_arg0 = .no_expand;

    child.spawn() catch |err| {
        ReportSvcStatus(winapi.SERVICE_STOPPED, @errorToInt(err), 0);
        return;
    };

    while (true) {
        _ = windows.kernel32.WaitForSingleObject(g.hSvcStopEvent, windows.INFINITE);
        _ = child.kill() catch |err| {
            ReportSvcStatus(winapi.SERVICE_STOPPED, @errorToInt(err), 0);
            return;
        };
        return;
    }
}

pub fn ReportSvcStatus(
    dwCurrentState: windows.DWORD,
    dwWin32ExitCode: windows.DWORD,
    dwWaitHint: windows.DWORD,
) void {
    const static = struct {
        var dwCheckPoint: windows.DWORD = 1;
    };

    g.SvcStatus.dwCurrentState = dwCurrentState;
    g.SvcStatus.dwWin32ExitCode = dwWin32ExitCode;
    g.SvcStatus.dwWaitHint = dwWaitHint;

    if (dwCurrentState == winapi.SERVICE_START_PENDING) {
        g.SvcStatus.dwControlsAccepted = 0;
    } else {
        g.SvcStatus.dwControlsAccepted = winapi.SERVICE_ACCEPT_STOP;
    }

    if (dwCurrentState == winapi.SERVICE_RUNNING or
        dwCurrentState == winapi.SERVICE_STOPPED)
    {
        g.SvcStatus.dwCheckPoint = 0;
    } else {
        static.dwCheckPoint += 1;
        g.SvcStatus.dwCheckPoint = static.dwCheckPoint;
    }

    _ = winapi.SetServiceStatus(g.SvcStatusHandle, &g.SvcStatus);
}

pub fn SvcReportEvent() void {
    var hEventSource = winapi.RegisterEventSourceW(null, g.SvcName) orelse {
        return;
    };
    defer winapi.DeregisterEventSource(hEventSource);
}
