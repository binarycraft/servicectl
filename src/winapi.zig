const std = @import("std");
const os = std.os;
const fmt = std.fmt;
const mem = std.mem;
const log = std.log;
const time = std.time;
const print = std.debug.print;
const unicode = std.unicode;
const windows = os.windows;

pub const SERVICE_NO_CHANGE = 0xffffffff;
pub const SERVICE_WIN32_OWN_PROCESS = 0x00000010;
pub const SERVICE_WIN32_SHARE_PROCESS = 0x00000020;
pub const SERVICE_WIN32 = (SERVICE_WIN32_OWN_PROCESS | SERVICE_WIN32_SHARE_PROCESS);

pub const SERVICE_INTERACTIVE_PROCESS = 0x00000100;

pub const SERVICE_CONTROL_STOP = 0x00000001;
pub const SERVICE_CONTROL_PAUSE = 0x00000002;
pub const SERVICE_CONTROL_CONTINUE = 0x00000003;
pub const SERVICE_CONTROL_INTERROGATE = 0x00000004;
pub const SERVICE_CONTROL_SHUTDOWN = 0x00000005;
pub const SERVICE_CONTROL_PARAMCHANGE = 0x00000006;
pub const SERVICE_CONTROL_NETBINDADD = 0x00000007;
pub const SERVICE_CONTROL_NETBINDREMOVE = 0x00000008;
pub const SERVICE_CONTROL_NETBINDENABLE = 0x00000009;
pub const SERVICE_CONTROL_NETBINDDISABLE = 0x0000000A;
pub const SERVICE_CONTROL_DEVICEEVENT = 0x0000000B;
pub const SERVICE_CONTROL_HARDWAREPROFILECHANGE = 0x0000000C;
pub const SERVICE_CONTROL_POWEREVENT = 0x0000000D;
pub const SERVICE_CONTROL_SESSIONCHANGE = 0x0000000E;

pub const SERVICE_STOPPED = 0x00000001;
pub const SERVICE_START_PENDING = 0x00000002;
pub const SERVICE_STOP_PENDING = 0x00000003;
pub const SERVICE_RUNNING = 0x00000004;
pub const SERVICE_CONTINUE_PENDING = 0x00000005;
pub const SERVICE_PAUSE_PENDING = 0x00000006;
pub const SERVICE_PAUSED = 0x00000007;

pub const SERVICE_ACCEPT_STOP = 0x00000001;
pub const SERVICE_ACCEPT_PAUSE_CONTINUE = 0x00000002;
pub const SERVICE_ACCEPT_SHUTDOWN = 0x00000004;
pub const SERVICE_ACCEPT_PARAMCHANGE = 0x00000008;
pub const SERVICE_ACCEPT_NETBINDCHANGE = 0x00000010;
pub const SERVICE_ACCEPT_HARDWAREPROFILECHANGE = 0x00000020;
pub const SERVICE_ACCEPT_POWEREVENT = 0x00000040;
pub const SERVICE_ACCEPT_SESSIONCHANGE = 0x00000080;

pub const STANDARD_RIGHTS_REQUIRED = 0x000F0000;
pub const SERVICE_BOOT_START = 0x00000000;
pub const SERVICE_SYSTEM_START = 0x00000001;
pub const SERVICE_AUTO_START = 0x00000002;
pub const SERVICE_DEMAND_START = 0x00000003;
pub const SERVICE_DISABLED = 0x00000004;
pub const SERVICE_ERROR_IGNORE = 0x00000000;
pub const SERVICE_ERROR_NORMAL = 0x00000001;
pub const SERVICE_ERROR_SEVERE = 0x00000002;
pub const SERVICE_ERROR_CRITICAL = 0x00000003;

pub const SC_MANAGER_CONNECT = 0x0001;
pub const SC_MANAGER_CREATE_SERVICE = 0x0002;
pub const SC_MANAGER_ENUMERATE_SERVICE = 0x0004;
pub const SC_MANAGER_LOCK = 0x0008;
pub const SC_MANAGER_QUERY_LOCK_STATUS = 0x0010;
pub const SC_MANAGER_MODIFY_BOOT_CONFIG = 0x0020;

pub const SC_MANAGER_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED | SC_MANAGER_CONNECT |
    SC_MANAGER_CREATE_SERVICE | SC_MANAGER_ENUMERATE_SERVICE |
    SC_MANAGER_LOCK | SC_MANAGER_QUERY_LOCK_STATUS | SC_MANAGER_MODIFY_BOOT_CONFIG;

pub const SERVICE_QUERY_CONFIG = 0x0001;
pub const SERVICE_CHANGE_CONFIG = 0x0002;
pub const SERVICE_QUERY_STATUS = 0x0004;
pub const SERVICE_ENUMERATE_DEPENDENTS = 0x0008;
pub const SERVICE_START = 0x0010;
pub const SERVICE_STOP = 0x0020;
pub const SERVICE_PAUSE_CONTINUE = 0x0040;
pub const SERVICE_INTERROGATE = 0x0080;
pub const SERVICE_USER_DEFINED_CONTROL = 0x0100;

pub const SERVICE_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED | SERVICE_QUERY_CONFIG |
    SERVICE_CHANGE_CONFIG | SERVICE_QUERY_STATUS | SERVICE_ENUMERATE_DEPENDENTS |
    SERVICE_START | SERVICE_STOP | SERVICE_PAUSE_CONTINUE | SERVICE_INTERROGATE |
    SERVICE_USER_DEFINED_CONTROL;

pub const SERVICE_STATUS_HANDLE = windows.HANDLE;
pub const SC_HANDLE = windows.HANDLE;

pub const LPSERVICE_MAIN_FUNCTIONW = fn (
    dwNumServicesArgs: windows.DWORD,
    lpServiceArgVectors: ?*windows.LPWSTR,
) callconv(windows.WINAPI) void;

pub const LPHANDLER_FUNCTION = fn (
    dwControl: windows.DWORD,
) callconv(windows.WINAPI) void;

pub const SERVICE_TABLE_ENTRYW = struct {
    lpServiceName: ?windows.LPWSTR,
    lpServiceProc: ?*const LPSERVICE_MAIN_FUNCTIONW,
};

pub const SERVICE_STATUS = struct {
    dwServiceType: windows.DWORD,
    dwCurrentState: windows.DWORD,
    dwControlsAccepted: windows.DWORD,
    dwWin32ExitCode: windows.DWORD,
    dwServiceSpecificExitCode: windows.DWORD,
    dwCheckPoint: windows.DWORD,
    dwWaitHint: windows.DWORD,
};

pub const SERVICE_STATUS_PROCESS = struct {
    dwServiceType: windows.DWORD,
    dwCurrentState: windows.DWORD,
    dwControlsAccepted: windows.DWORD,
    dwWin32ExitCode: windows.DWORD,
    dwServiceSpecificExitCode: windows.DWORD,
    dwCheckPoint: windows.DWORD,
    dwWaitHint: windows.DWORD,
    dwProcessId: windows.DWORD,
    dwServiceFlags: windows.DWORD,
};

pub const SC_STATUS_TYPE = enum(c_int) {
    SC_STATUS_PROCESS_INFO,
};

pub const QUERY_SERVICE_CONFIGW = struct {
    dwServiceType: windows.DWORD,
    dwStartType: windows.DWORD,
    dwErrorControl: windows.DWORD,
    lpBinaryPathName: ?windows.LPWSTR,
    lpLoadOrderGroup: ?windows.LPWSTR,
    dwTagId: windows.DWORD,
    lpDependencies: ?windows.LPWSTR,
    lpServiceStartName: ?windows.LPWSTR,
    lpDisplayName: ?windows.LPWSTR,
};

pub const SERVICE_DESCRIPTIONW = struct {
    lpDescription: ?windows.LPWSTR,
};

pub const SERVICE_CONFIG_DESCRIPTION = 1;

pub extern "advapi32" fn RegisterServiceCtrlHandlerW(
    lpServiceName: ?windows.LPCWSTR,
    lpHandlerProc: ?*const LPHANDLER_FUNCTION,
) callconv(windows.WINAPI) ?SERVICE_STATUS_HANDLE;

pub extern "advapi32" fn SetServiceStatus(
    hServiceStatus: ?SERVICE_STATUS_HANDLE,
    lpServiceStatus: ?*SERVICE_STATUS,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn StartServiceCtrlDispatcherW(
    lpServiceStartTable: ?*SERVICE_TABLE_ENTRYW,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn OpenSCManagerW(
    lpMachineName: ?windows.LPCWSTR,
    lpDatabaseName: ?windows.LPCWSTR,
    dwDesiredAccess: windows.DWORD,
) callconv(windows.WINAPI) ?SC_HANDLE;

pub extern "advapi32" fn CreateServiceW(
    hSCManager: ?SC_HANDLE,
    lpServiceName: ?windows.LPCWSTR,
    lpDisplayName: ?windows.LPCWSTR,
    dwDesiredAccess: windows.DWORD,
    dwServiceType: windows.DWORD,
    dwStartType: windows.DWORD,
    dwErrorControl: windows.DWORD,
    lpBinaryPathName: ?windows.LPCWSTR,
    lpLoadOrderGroup: ?windows.LPCWSTR,
    lpdwTagId: ?windows.LPCWSTR,
    lpDependencies: ?windows.LPCWSTR,
    lpServiceStartName: ?windows.LPCWSTR,
    lpPassword: ?windows.LPCWSTR,
) callconv(windows.WINAPI) ?SC_HANDLE;

pub extern "advapi32" fn DeleteService(
    hService: SC_HANDLE,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn OpenServiceW(
    hSCManager: SC_HANDLE,
    lpServiceName: windows.LPCWSTR,
    dwDesiredAccess: windows.DWORD,
) callconv(windows.WINAPI) ?SC_HANDLE;

pub extern "advapi32" fn CloseServiceHandle(
    hSCObject: SC_HANDLE,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "kernel32" fn CreateEventW(
    lpEventAttributes: ?*windows.SECURITY_ATTRIBUTES,
    bManualReset: windows.BOOLEAN,
    bInitialState: windows.BOOLEAN,
    lpName: ?windows.LPCWSTR,
) callconv(windows.WINAPI) ?windows.HANDLE;

pub extern "kernel32" fn SetEvent(
    hEvent: windows.HANDLE,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn RegisterEventSourceW(
    lpUNCServerName: ?windows.LPCWSTR,
    lpSourceName: ?windows.LPCWSTR,
) callconv(windows.WINAPI) ?windows.HANDLE;

pub extern "advapi32" fn DeregisterEventSource(
    hEventLog: windows.HANDLE,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn ReportEventW(
    hEventLog: ?windows.HANDLE,
    wType: windows.WORD,
    wCategory: windows.WORD,
    dwEventID: windows.DWORD,
    lpUserSid: ?windows.PVOID,
    wNumStrings: windows.WORD,
    dwDataSize: windows.DWORD,
    lpStrings: ?*windows.LPCWSTR,
    lpRawData: ?windows.LPVOID,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn QueryServiceStatusEx(
    hService: SC_HANDLE,
    InfoLevel: SC_STATUS_TYPE,
    lpBuffer: *u8,
    cbBufSize: windows.DWORD,
    pcbBytesNeeded: ?*windows.DWORD,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn QueryServiceConfigW(
    hService: SC_HANDLE,
    lpServiceConfig: ?*QUERY_SERVICE_CONFIGW,
    cbBufSize: windows.DWORD,
    pcbBytesNeeded: ?*windows.DWORD,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn QueryServiceConfig2W(
    hService: SC_HANDLE,
    dwInfoLevel: windows.DWORD,
    lpBuffer: ?*u8,
    cbBufSize: windows.DWORD,
    pcbBytesNeeded: ?*windows.DWORD,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn StartServiceW(
    hService: SC_HANDLE,
    dwNumServiceArgs: windows.DWORD,
    lpServiceArgVectors: ?*windows.LPCWSTR,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn ControlService(
    hService: SC_HANDLE,
    dwControl: windows.DWORD,
    lpServiceStatus: ?*SERVICE_STATUS,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "advapi32" fn ChangeServiceConfigW(
    hService: SC_HANDLE,
    dwServiceType: windows.DWORD,
    dwStartType: windows.DWORD,
    dwErrorControl: windows.DWORD,
    lpBinaryPathName: ?windows.LPCWSTR,
    lpLoadOrderGroup: ?windows.LPCWSTR,
    lpdwTagId: ?windows.LPCWSTR,
    lpDependencies: ?windows.LPCWSTR,
    lpServiceStartName: ?windows.LPCWSTR,
    lpPassword: ?windows.LPCWSTR,
    lpDisplayName: ?windows.LPCWSTR,
) callconv(windows.WINAPI) windows.BOOLEAN;

pub extern "kernel32" fn GetTickCount() callconv(windows.WINAPI) windows.DWORD;
