const std = @import("std");
const builtin = @import("builtin");
const os = std.os;
const fmt = std.fmt;
const mem = std.mem;
const time = std.time;
const print = std.debug.print;
const windows = os.windows;
const errno = os.system.getErrno;

pub const pid_t = windows.DWORD;
pub const RTL_CLONE_PROCESS_FLAGS_CREATE_SUSPENDED = 0x00000001;
pub const RTL_CLONE_PROCESS_FLAGS_INHERIT_HANDLES = 0x00000002;

pub const CLIENT_ID = extern struct {
    UniqueProcess: windows.LPVOID,
    UniqueThread: windows.LPVOID,
};

pub const SECTION_IMAGE_INFORMATION = extern struct {
    EntryPoint: windows.LPVOID,
    StackZeroBits: c_ulong,
    StackReserved: c_ulong,
    StackCommit: c_ulong,
    ImageSubsystem: c_ulong,
    SubSystemVersionLow: windows.DWORD,
    SubSystemVersionHigh: windows.DWORD,
    Unknown1: c_ulong,
    ImageCharacteristics: c_ulong,
    ImageMachineType: c_ulong,
    Unknown2: [3]c_ulong,
};

pub const RTL_USER_PROCESS_INFORMATION = extern struct {
    ulSize: c_ulong,
    hProcess: windows.HANDLE,
    hThread: windows.HANDLE,
    ClientId: CLIENT_ID,
};

pub extern "c" fn daemon(nochdir: usize, noclose: usize) usize;

pub extern "ntdll" fn RtlCloneUserProcess(
    ulProcessFlags: c_ulong,
    lpProcessAttributes: ?*windows.SECURITY_ATTRIBUTES,
    lpThreadAttributes: ?*windows.SECURITY_ATTRIBUTES,
    DebugPort: ?*windows.HANDLE,
    lpProcessInformation: *RTL_USER_PROCESS_INFORMATION,
) callconv(windows.WINAPI) windows.NTSTATUS;

pub extern "kernel32" fn AllocConsole() callconv(windows.WINAPI) c_uint;

pub extern "kernel32" fn OpenProcess(
    dwDesiredAccess: windows.DWORD,
    bInheritHandle: windows.BOOLEAN,
    dwProcessId: windows.DWORD,
) callconv(windows.WINAPI) ?windows.HANDLE;

pub extern "kernel32" fn GetProcessId(
    Process: windows.HANDLE,
) windows.DWORD;

pub extern "kernel32" fn GetThreadId(
    Thread: windows.HANDLE,
) windows.DWORD;

pub extern "kernel32" fn OpenThread(
    dwDesiredAccess: windows.DWORD,
    bInheritHandle: windows.BOOLEAN,
    dwThreadId: windows.DWORD,
) callconv(windows.WINAPI) ?windows.HANDLE;

pub extern "kernel32" fn ResumeThread(
    hThread: windows.HANDLE,
) callconv(windows.WINAPI) windows.DWORD;

pub fn forkW() !pid_t {
    var piProcInfo: RTL_USER_PROCESS_INFORMATION = undefined;
    const status = RtlCloneUserProcess(
        RTL_CLONE_PROCESS_FLAGS_CREATE_SUSPENDED |
            RTL_CLONE_PROCESS_FLAGS_INHERIT_HANDLES,
        null,
        null,
        null,
        &piProcInfo,
    );
    var pid: windows.DWORD = undefined;
    if (status == windows.NTSTATUS.SUCCESS) {
        //_ = windows.kernel32.GetCurrentProcess();
        const processID = @intCast(windows.DWORD, @ptrToInt(piProcInfo.ClientId.UniqueProcess));
        const threadID = @intCast(windows.DWORD, @ptrToInt(piProcInfo.ClientId.UniqueThread));

        const processHandle = OpenProcess(
            windows.STANDARD_RIGHTS_REQUIRED | windows.SYNCHRONIZE | 0xffff,
            windows.FALSE,
            processID,
        ) orelse {
            switch (windows.kernel32.GetLastError()) {
                .INVALID_PARAMETER => unreachable,
                else => |err| return windows.unexpectedError(err),
            }
        };
        defer windows.CloseHandle(processHandle);

        const threadHandle = OpenThread(
            windows.STANDARD_RIGHTS_REQUIRED | windows.SYNCHRONIZE | 0xffff,
            windows.FALSE,
            threadID,
        ) orelse {
            switch (windows.kernel32.GetLastError()) {
                .INVALID_PARAMETER => unreachable,
                else => |err| return windows.unexpectedError(err),
            }
        };
        defer windows.CloseHandle(threadHandle);

        if (ResumeThread(threadHandle) == -1) {
            switch (windows.kernel32.GetLastError()) {
                .FILE_NOT_FOUND => return error.FileNotFound,
                .PATH_NOT_FOUND => return error.FileNotFound,
                .ACCESS_DENIED => return error.AccessDenied,
                .INVALID_PARAMETER => unreachable,
                .INVALID_NAME => return error.InvalidName,
                else => |err| return windows.unexpectedError(err),
            }
        }

        pid = processID;
    } else if (status == windows.NTSTATUS.PROCESS_CLONED) {
        if (AllocConsole() == 0) {
            switch (windows.kernel32.GetLastError()) {
                .FILE_NOT_FOUND => return error.FileNotFound,
                .PATH_NOT_FOUND => return error.FileNotFound,
                .ACCESS_DENIED => return error.AccessDenied,
                .INVALID_PARAMETER => unreachable,
                .INVALID_NAME => return error.InvalidName,
                else => |err| return windows.unexpectedError(err),
            }
        }

        pid = @intCast(pid_t, 0);
    } else {
        switch (windows.kernel32.GetLastError()) {
            .FILE_NOT_FOUND => return error.FileNotFound,
            .PATH_NOT_FOUND => return error.FileNotFound,
            .ACCESS_DENIED => return error.AccessDenied,
            .INVALID_PARAMETER => unreachable,
            .INVALID_NAME => return error.InvalidName,
            else => |err| return windows.unexpectedError(err),
        }
    }
    return pid;
}

pub fn becomeDaemon(nochdir: usize, noclose: usize) !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const argv = try std.process.argsAlloc(gpa.allocator());
    defer std.process.argsFree(gpa.allocator(), argv);

    if (builtin.os.tag == .windows) {
        _ = try forkW();
    } else {
        const rc = daemon(nochdir, noclose);
        switch (errno(rc)) {
            .SUCCESS => return,
            .AGAIN => return error.SystemResources,
            .NOMEM => return error.SystemResources,
            .PERM => return error.PermissionDenied,
            else => |err| return os.unexpectedErrno(err),
        }
    }
}
